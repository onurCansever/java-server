import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//Version 1.0.2
public class Main {
    static int port = 5000;
    static ServerSocket serverSocket;
    static Socket socket;

    public static void main(String[] args) {
        try {
            //create tcp server
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started.");
            System.out.println("Server version is 1.0.2");
            

            //start server
            while(true) {
                socket = serverSocket.accept();
                System.out.println("New client connected.");
                new TcpListenerThread(socket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
