FROM alpine:latest
WORKDIR /javaServer
EXPOSE 5000/tcp
RUN apk update
RUN apk upgrade
RUN apk add openjdk17-jre
COPY *.class ./
CMD ["java", "Main"]