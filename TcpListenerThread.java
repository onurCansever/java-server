import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class TcpListenerThread extends Thread {
    Socket socket;
    DataOutputStream out;


    public TcpListenerThread(Socket socket) {
        this.socket = socket;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String message = "this is test message";
        
        while (true) {
            try {
                out.writeUTF(message);
                System.out.println("message sent.");
                sleep(2000);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("client disconnected");
                break;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }
}